###########################
##  Archinstall  Script  ##
##  Tested May 15, 2024  ##
###########################
---------------------------
## At the prompt, start Arch Installer
archinstall

## Fill out the menu and then start the install.
## When Completed, you don't need to log into chroot when asked Just restart the computer.

## Finish Up
exit
umount -R /mnt
reboot

EXTRAS
--At GRUB Boot Screen type "e" to edit grub menu
--Arrow down to linux line and add "video=1920x1080" to the end of the line (without the quotes)
--CTRL x to reboot

Login
setfont ter-132n

Check Internet connection
ip -c a

ping google.com

## Install Window Managers
sudo -S pacman xorg-server xorg-xrandr i3 cinnamon dmenu lxappearence nitrogen feh pcmanfm ranger firefox sddm lightdm-gtk-greeter lightdm-gtk-greeter-settings terminator alacritty

sudo systemctl enable lightdm (or sddm)
reboot

#For i3 installations
Login with i3
Enter to Create Config file
Enter to accept Mod Key

Mod Key+Enter Opens Terminator (default terminal)

## EXTRA THINGS TO INSTALL:
sudo pacman -S geany arandr exa

##Install Paru (AUR Helper)
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si

## CREATE SWAP FILE
- fallocate -l 2g /swapfile
- chmod 600 /swapfile
- mkswap /swapfile
- cp /etc/fstab /etc/fstab.bak
- echo '/swapfile none swap 0 0' | tee -a /etc/fstab
- cat /etc/fstab

##WAYMONAD INSTALL
sudo pacman -S wlroots stack
--Installs Haskell packages wor waymonad.

git clone --recursive https://github.com/ongy/waymonad
cd waymonad
[PKG_CONFIG_PATH=/usr/local/lib/pkgconfig] cabal new-build
