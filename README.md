# Arch Linux Installation Instructions

The Arch Linux Install instructions were recently updated in May 2022.

You may follow these directions under your own discretion. I WILL NOT make any promises that this will work on your machine.

They work fine on machines I've installed Arch Linux on and in VMs as well. These instructions are written so that I can understand them. 

I am not opposed to suggested changes. I may implement them or I may not. In the end, it is my decision. 

I'm in the US so these instructions are based on US settings.

If you're uncomfortable using these instructions, it's your choice not to do so.

The hard drive partition sections are for NEW/UNUSED hard drives ONLY! Partitioning using these instructions WILL destroy any data on used drives!

These instructions are not designed for dual booting Windows on the same drive. The Windows partition COULD be destroyed if you use these instructions for dual booting purposes!

Use at your Own Risk!
